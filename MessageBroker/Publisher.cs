﻿using System.Collections.Generic;

namespace MessageBroker
{
	internal class Publisher
	{
		protected HashSet<ISubscriber> _subscribers = new HashSet<ISubscriber>();


		internal void Post(IMessage message)
		{
			foreach (var subscriber in _subscribers)
			{
				subscriber.HandleMessage(message);
			}
		}


		internal void Subscribe(ISubscriber subscriber)
		{
			_subscribers.Add(subscriber);
		}


		internal void Unsubscribe(ISubscriber subscriber)
		{
			_subscribers.Remove(subscriber);
		}
	}
}