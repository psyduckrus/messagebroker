﻿namespace MessageBroker
{
	internal interface ISubscriber
	{
		void HandleMessage(IMessage message);
	}
}